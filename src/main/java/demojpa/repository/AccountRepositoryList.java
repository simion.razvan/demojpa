package demojpa.repository;

import demojpa.domain.Account;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class AccountRepositoryList {


    public List<Account> allAccounts() {
        List<Account> accounts = new ArrayList<>();
        Account a1 = new Account("a1", BigInteger.valueOf(100));
        Account a2 = new Account("a2", BigInteger.valueOf(200));
        accounts.add(a1);
        accounts.add(a2);
        return accounts;
    }

    public Account save(Account account) {
        //todo implement
        return account;
    }

    public Account update(Account account) {
        //todo implement
        return account;
    }

    public Account findById(Long id) {
        //todo implement
        return null;
    }


}
