package demojpa.domain;

import java.math.BigInteger;


public class Account {

    private String accountNr;
    private BigInteger balance;

    public Account() {
    }

    public Account(String accountNr, BigInteger balance) {
        this.accountNr = accountNr;
        this.balance = balance;
    }


    public String getAccountNr() {
        return accountNr;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public Long getId(){
        //todo implement with JPA
        return null;
    }


}
