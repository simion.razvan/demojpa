package demojpa.service;

import demojpa.domain.Account;
import demojpa.repository.AccountRepositoryList;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import java.math.BigInteger;
import java.util.List;


public class Banking {

    private AccountRepositoryList accountRepository = new AccountRepositoryList();


    public List<Account> allAccounts() {
        return accountRepository.allAccounts();
    }

    public Account create(Account account) {
        return accountRepository.save(account);
    }

    public Account findById(Long id) {
        return accountRepository.findById(id);
    }



}
