package demojpa.resource;

import demojpa.domain.*;
import demojpa.service.Banking;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.math.BigInteger;
import java.net.URI;
import java.util.List;

@Path("/accounts")
public class AccountResource {

    @Context
    private UriInfo uriInfo;

    private Banking bankingService = new Banking();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response allAccounts() {
        GenericEntity<List<Account>> accounts = new GenericEntity<>(bankingService.allAccounts()) {
        };
        return Response.ok(accounts).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@PathParam("id") Long id) {
        Account account = bankingService.findById(id);
        return Response.ok(account).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(Account account) {
        Account saved = bankingService.create(account);
        String url = uriInfo.getAbsolutePath() + "/" + saved.getId();
        URI uri = URI.create(url);
        return Response.created(uri).build();
    }


}
